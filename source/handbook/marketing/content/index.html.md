---
layout: markdown_page
title: "Content Team"
---

The Content Team aka the "Blog, Documentation and Webinar Team" is part of the
Marketing Team and specializes in creating and facilitating the creation of
content and discussions for the blog, documentation and YouTube webinars.

## Who creates blog, documentation and webinar content for GitLab?

The Content Team is not the only source of content for these channels,
all GitLab team members and the extended community are encouraged to contribute
for the following reasons:

- The Content Team's resources are limited; GitLab is a large company with
  a comprehensive and constantly improving product suite in the fast paced
  industry of software development. Because of this we cannot be
  aware of every possible development inside GitLab, the community and e.
- The voice of GitLab is not just that of the Content Team, it is the
  contribution of every GitLab team member, community member, partner, customer
  and user and we want to share all these perspectives.
- The Content Team needs your help to create content and suggest content ideas
  so that the GitLab team and extended community can stay ahead of the curve.

## What areas of "content" does the Content Team specialize in?

- **Blog:** see the team page for each Content Team members' content specialties.
  Each team member will create content and facilitate others contributing
  content to their area of specialty. A general theme we are focusing on is
  'User Driven Content'.
- **Documentation:** creating Git and GitLab lessons, maintaining the structure and
  style of articles and working with the development team and community to
  facilitate the creation and updating of articles when features are added and
  changed.
- **YouTube webinars:** focusing on feature and release previews and GitLab team
  member AMAs.
- **Bi-weekly newsletter:** the creation of the bi-weekly newsletter on the
  8th and 22nd (release newsletter) of each month. We welcome all teams to
  suggest content to include in newsletters.
- **Content Team social promotion:** Each Content Team member is responsible for
  the social promotion of all content in their area of specialty. Content team
  members should share content on Facebook, Twitter, LinkedIn, Reddit (where
  applicable) and occasionally Hacker News - most of the time we want the
  community to share our content on Hacker News rather than us so that our
  presence feels organic rather than forcefully marketed.

## Specific areas of "content" the Content Team does not own:

- **Website**
- **PR**
- **Outbound marketing**
- **White papers**
- **Training & support:** outside of Git and GitLab lesson in the documentation.
- **Sales:** outside of indirect content on our channels support the Sales Team
  or generate MQLs.
- **Handbook:** each team owns their section of the handbook.
